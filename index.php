<?php
define('ACCESS_TOKEN','');
define('CRUSH_USER_ID', '');
define('YOUR_ID', '');
define('GRAPH_URL','https://graph.fb.me/');

$list_reaction = ['LIKE','LOVE','WOW','HAHA','ANGRY'];
$posts = json_decode(request(GRAPH_URL.CRUSH_USER_ID.'/posts?fields=id,reactions&limit=1&access_token='.ACCESS_TOKEN),true);
$crushPostID = $posts['data'][0]['id'];
$reactions = $list_reaction[array_rand($list_reaction)];

$log = fopen('log.txt', 'a+');
fwrite($log, $crushPostID . "\n");
fclose($log);

$url = GRAPH_URL.$crushPostID.'/reactions?type='.$reactions.'&method=POST&access_token='.ACCESS_TOKEN;
$check = json_decode(file_get_contents($url),true);
echo $check['success'] == 'true' ? "POST ID : $crushPostID <br> REACTION TYPE : $reactions " : "Fail";
function request($url)
{
    if (!filter_var($url, FILTER_VALIDATE_URL)) {
        return FALSE;
    }
    
    $options = array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_HEADER => FALSE,
        CURLOPT_FOLLOWLOCATION => TRUE,
        CURLOPT_ENCODING => '',
        CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.87 Safari/537.36',
        CURLOPT_AUTOREFERER => TRUE,
        CURLOPT_CONNECTTIMEOUT => 15,
        CURLOPT_TIMEOUT => 15,
        CURLOPT_MAXREDIRS => 5,
        CURLOPT_SSL_VERIFYHOST => 2,
        CURLOPT_SSL_VERIFYPEER => 0
    );
    
    $ch = curl_init();
    curl_setopt_array($ch, $options);
    $response  = curl_exec($ch);
    $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    unset($options);
    return $http_code === 200 ? $response : FALSE;
}
?>